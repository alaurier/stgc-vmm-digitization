# sTGC VMM Digitization
This page details the most recent implementation of the sTGC VMM model found in the digitization package of the athena framework, developped by Alexandre Laurier and integrated into athena as of November 20th 2022. The sTGC digitization package in athena can be found [here](https://gitlab.cern.ch/atlas/athena/-/tree/master/MuonSpectrometer/MuonDigitization/sTGC_Digitization). The creation of digits in the [sTGCDigitMaker](https://gitlab.cern.ch/atlas/athena/-/blob/master/MuonSpectrometer/MuonDigitization/sTGC_Digitization/src/sTgcDigitMaker.cxx) are not discussed here, and instead the focus is given to the treatment of digits by the VMM.

The first official discussion and presentation of the new logic and results can be found in the Muon SW meeting [here](https://indico.cern.ch/event/1220592/contributions/5134719/attachments/2545292/4383052/sTGC_Digi_Updates_alaurier_2022-11-10.pdf). Three sets of slides are contained in this git project that should include more detail.

### Version history
This is the second complete version of the simulation of the sTGC VMM. The updated digitization package accurately describes the current electronics of the VMM3. In contrast, the previous implementation had become outdated and did not reflect the current state of the technology, but instead described the old VMM2. The current digitization package offers a more comprehensive and up-to-date representation of the sTGC VMM3.

## Basic Logic of the Digitization Package
* On an event by event basis, HITS left by particles travelling through the sTGC are processed through the [sTgcDigitzationTool](https://gitlab.cern.ch/atlas/athena/-/blob/master/MuonSpectrometer/MuonDigitization/sTGC_Digitization/src/sTgcDigitizationTool.cxx).
* The collection of HITS are [processed](https://gitlab.cern.ch/atlas/athena/-/blob/master/MuonSpectrometer/MuonDigitization/sTGC_Digitization/src/sTgcDigitizationTool.cxx#L506) through the [sTGCDigitMaker](https://gitlab.cern.ch/atlas/athena/-/blob/master/MuonSpectrometer/MuonDigitization/sTGC_Digitization/src/sTgcDigitMaker.cxx) in order to create digits. Digits roughly correspond to an electronic signal to be processed by the VMM with information such as channel number, channel type, charge and time.
* The resulting digits are processed by the [VMM](https://gitlab.cern.ch/atlas/athena/-/blob/master/MuonSpectrometer/MuonDigitization/sTGC_Digitization/src/sTgcDigitizationTool.cxx#L793) to replicate as best as possible the behavior of the real electronics.
* After processing, digits that pass the VMM criteria on charge threshold and deadtime are [saved to output containers](https://gitlab.cern.ch/atlas/athena/-/blob/master/MuonSpectrometer/MuonDigitization/sTGC_Digitization/src/sTgcDigitizationTool.cxx#L793) to be converted to RDOs, marking the end of the digitization.

## The Digitization Tool and the Handling of Digits
The steps described below happen after the HITS collection is digitized by the [sTGCDigitMaker](https://gitlab.cern.ch/atlas/athena/-/blob/master/MuonSpectrometer/MuonDigitization/sTGC_Digitization/src/sTgcDigitMaker.cxx).
* The raw digits are temporarily stored to [containers](https://gitlab.cern.ch/atlas/athena/-/blob/master/MuonSpectrometer/MuonDigitization/sTGC_Digitization/src/sTgcDigitizationTool.cxx#L300) along side the simData from the SimHit. The SimHits are carried along in this step to properly create the SDOs (Simulated Data Object) that are used for truth matching in MC. Three maps are used: one each for strips, pads, and wires:
```
// Collections of digits by digit type associated with a detector element
std::map< Identifier, std::map< Identifier, std::vector<sTgcSimDigitData> > > unmergedPadDigits;
std::map< Identifier, std::map< Identifier, std::vector<sTgcSimDigitData> > > unmergedStripDigits;
std::map< Identifier, std::map< Identifier, std::vector<sTgcSimDigitData> > > unmergedWireDigits;
```
The maps are organized by layer (via layerID) and channel (via channelID). In the following [example](https://gitlab.cern.ch/atlas/athena/-/blob/master/MuonSpectrometer/MuonDigitization/sTGC_Digitization/src/sTgcDigitizationTool.cxx#L578), a pair of type <sTGCSimData, sTGCDigit> is inserted into a vector containing all the pad digits of the layer with unique identifier layerID and given pad with unique identifer newDigitId.
```
unmergedPadDigits[layerID][newDigitId].emplace_back(simData, newDigit);
```

* For each of the three channel types, all the digits are processed by the VMM through the [processDigitsWithVMM](https://gitlab.cern.ch/atlas/athena/-/blob/master/MuonSpectrometer/MuonDigitization/sTGC_Digitization/src/sTgcDigitizationTool.cxx#L793) function. The processDigitsWithVMM function processes all the digits of a given channel type in a single layer simultaneously. The detailed explanation of this function is explained below.
* For each readout channel with a digit that passed the VMM selection, a SDO is [created](processDigitsWithVMM) and the resulting digit is temporarly stored in a [container of digits](https://gitlab.cern.ch/atlas/athena/-/blob/master/MuonSpectrometer/MuonDigitization/sTGC_Digitization/src/sTgcDigitizationTool.cxx#L635). The SDOs and saved digits are then output from the digitization job and the digitization is finished.

## The VMM - The processDigitsWithVMM function
The [processDigitsWithVMM](https://gitlab.cern.ch/atlas/athena/-/blob/master/MuonSpectrometer/MuonDigitization/sTGC_Digitization/src/sTgcDigitizationTool.cxx#L793) function describes the full functionallity of the VMM3s for the sTGC, including:
1. Hit merging: digits close in time (within the time integration window) are physically read out as one signal and not several.
2. Deadtime: the VMM electronics, after having recently been saved a signal, are dead and cant not be read out for a period of time of several hundreds of nanoseconds.
3. Charge threshold: a given charge threshold is applied on the readout channels, below which charge is not read out by the VMM.
4. NeighborOn functionally for strips: strips above charge threshold will trigger the immediate neighboring strips to also be read out by the VMM, even if they are below threshold.

Prior to discussing the implementation of each of the four main functions of the VMM, some information is needed to help rationalize the logic. First, the description of the VMM must follow a time-ordered logic to properly describe the real VMM electronics. One must get a precise time-ordered idea of what happens to the digits in order to properly capture the effects of the the time-dependent VMM. Second and last of all, the neighborOn functionnality of the strips is a layer-by-layer effect. The VMM needs to have access to neighboring strips on the same layer in order to evaluate the logical decision for the read out of these strips. The result of these two facts is that the processDigitsWithVMM function processes all the digits of a given layer simultaneously, and that the input digits need to be time-ordered.

From this point on, the following explanations will assume that all digits processed by a call of the processDigitsWithVMM function are from a given layer and channel type, and all the digits of a given channel are time-ordered. Each channel is processed one at the time in the next steps, i.e. if a given strip has multiple digits, they are all processed before the next strip.

The processDigitsWithVMM function returns a map linking unique channel identifiers to their pair of simData (for the SDO) and output digits. We will call this container saveDigits. The digits and simData of a given channel with identifer channelID will be then given by
```
savedDigits[channelID]
```


### 1. Hit merging
When two digits are found on the same channel within a small window of time called the [time integration window](https://gitlab.cern.ch/atlas/athena/-/blob/master/MuonSpectrometer/MuonDigitization/sTGC_Digitization/sTGC_Digitization/sTgcDigitizationTool.h#L155), they are combined into one unique digit. This time window is currently set to 30 ns and follows from the resolution of the peak finding discriminator of the VMM3. The processDigitsWithVMM performs hit merging between digits as its first step [here](https://gitlab.cern.ch/atlas/athena/-/blob/master/MuonSpectrometer/MuonDigitization/sTGC_Digitization/src/sTgcDigitizationTool.cxx#L807). 

Following our best understanding of the VMM, if the first digit in time is above the charge threshold (see point 3 below), the time of the new digit is simply the time of the first digit, and the new charge is the linear sum of the two charges. In the case where the first digit is not above the charge threshold, then the time at which the digit hits above threshold must be some sort average on the times of the summed digits. This is implemented as a weighed averaged of all summed digits [here](https://gitlab.cern.ch/atlas/athena/-/blob/master/MuonSpectrometer/MuonDigitization/sTGC_Digitization/src/sTgcDigitizationTool.cxx#L821). In this case, the charge of the merged digit is still the linear sum of the charge of the merged digits. A toy code summarizes the implmentation below:

```
if (digit1.charge > charge_threshold){
    // First case: digit1 is above charge threshold
    // Real VMM would use the time at which the charge first hits threshold
    merged_digit.time = digit1.time;
    merged_digit.charge = digit1.charge + digit2.charge;
}
else{
    // The two charge distributions are summe
    // Sometime between the two nominal times is when the charge reaches above treshold and triggers the VMM. 
    // Used weighed average to describe merged time
    merged_digit.time = (digit1.time*digit1.charge + digit2.time*digit2.charge) / (digit1.charge + digit2.charge)
    merged_digit.charge = digit1.charge + digit2.charge;
}

```

Between the start and end of this step, the number of digits did not change. The charge on a channel and the VMM is a continuous function of time and there is no valid reason or logic to delete digits that were merged into previous ones. The first digits in time are modified so that their new charge and time reflect the above logic, but they are never duplicated or deleted. If a "merged" digit should be deleted due to deadtime, then it will be skipped during the deadtime aspect of the VMM simulation. Otherwise, we run the risk of mistakenly removing a digit that should be still read out by the electronics.

### 2. Deadtime
After the digits are merged, the next logical step is to verify if the VMM is in its deadtime window, or rather if the VMM is free to save digits if they are valid. As a matter of fact, the VMM will be non-functional for the next several hundreds of nanoseconds after it reads out a digit, while it recovers. This deadtime window is currently set to [250 ns](https://gitlab.cern.ch/atlas/athena/-/blob/master/MuonSpectrometer/MuonDigitization/sTGC_Digitization/sTGC_Digitization/sTgcDigitizationTool.h#L143) for all three channels, although it may be closer to 300-350 ns.

The implementation of [deadtime](https://gitlab.cern.ch/atlas/athena/-/blob/master/MuonSpectrometer/MuonDigitization/sTGC_Digitization/src/sTgcDigitizationTool.cxx#L846) is a simple one in the current processDigitsWithVMM function. As mentionned above, we save all output digits into the savedDigits container. The simplest way to verify if a given channel can be saved by the VMM is to look at the output container for any digit with the same unique identifier with a time within the last deadtime window. This directly tells us if the VMM should be dead because it has recently saved a digit. **Reminder: Since the digits on a channel are time-ordered, so are the stored digits in the outputDigits.** If the VMM should be dead for the given channel of the digit, the digit is skipped and not saved. If the VMM is not dead, then the digit may progress through the next steps.

### 3. Charge threshold
Once it has been established that the VMM of a given digit is alive and can process the digit, the next step simply requires the digit to be above the charge threshold. If the VMM is indeed alive and the charge measured on the channel is larger than the threshold, then the digit is saved to the outputDigits container.

This is the end of the processDigitsWithVMM function for pads and strips. If the neighborOn functionnality is set to [false](https://gitlab.cern.ch/atlas/athena/-/blob/master/MuonSpectrometer/MuonDigitization/sTGC_Digitization/sTGC_Digitization/sTgcDigitizationTool.h#L146), then it is also the end of the VMM simulation for the strips.

### 4. NeighborOn
The neighborOn functionality of the strips is by far the trickiest implementation of the VMM. The following logic only applies to strip digits, only if neighborOn is set to [true](https://gitlab.cern.ch/atlas/athena/-/blob/master/MuonSpectrometer/MuonDigitization/sTGC_Digitization/sTGC_Digitization/sTgcDigitizationTool.h#L146).

The neighborOn functionality is simple to explain: if a strip is above threshold and on an alive VMM (ready to save the digit), then the VMM will force the immediate neighboring strips to also save their digits, assuming that the VMM on those neighbor strips are also alive.
In other words, if strip number N is above threshold and is saved by the VMM, then both strips with numbers N-1 and N+1 are also saved, assuming that their VMM allow for it.

This brings us to a logical idea of the implentation of the deadtime: if a strip is above threshold, look for its neighboring strips and if they can be saved, also save them. When testing, it was found that implementing this logic was the least efficient method. Instead, we follow the opposite (but equivalent!) method: only check below threshold strips for immediate neighbors that could force their read out. In fact, this logic agrees nicely with the implementation so far written out in the above steps 1 to 3 and requires few additions to the code.

The neighborOn functionnality is implemented [here](https://gitlab.cern.ch/atlas/athena/-/blob/master/MuonSpectrometer/MuonDigitization/sTGC_Digitization/src/sTgcDigitizationTool.cxx#L886). For strips above threshold, they follow the above steps 1 to 3, just like pads and wires. The implementation of neighborOn for below threshold strips is as follows:
 1. [Compute the unique strip identifier](https://gitlab.cern.ch/atlas/athena/-/blob/master/MuonSpectrometer/MuonDigitization/sTGC_Digitization/src/sTgcDigitizationTool.cxx#L873) of the above strip neighbor.
2. If the identifier is valid (the strip exists), get the charge threshold of the neighbor.
 3. Call the [neighborStripAboveThreshold](https://gitlab.cern.ch/atlas/athena/-/blob/master/MuonSpectrometer/MuonDigitization/sTGC_Digitization/src/sTgcDigitizationTool.cxx#L909) function that determines if the neighbor strip forces the read out of the strip. The function does:
    1. First, [looks in the outputDigits container](https://gitlab.cern.ch/atlas/athena/-/blob/master/MuonSpectrometer/MuonDigitization/sTGC_Digitization/src/sTgcDigitizationTool.cxx#L913) for an above threshold neighbor. If the container contains a digit, then it means the neighboring strip was already processed by the VMM and we only need to look at the outputDigits container for an above threshold digit that would trigger the read out of the current digit.
    2. If a digit in outputDigits is above threshold AND within the [time integration window](https://gitlab.cern.ch/atlas/athena/-/blob/master/MuonSpectrometer/MuonDigitization/sTGC_Digitization/sTGC_Digitization/sTgcDigitizationTool.h#L155), then the function returns true. Otherwise, the function returns false.
    3. If no digit is found in the outputDigits container, do the same kind of verification as 1. and 2. but in the set of unprocessed digits of the layer. If a yet unprocessed digit is observed to be above threshold within the time window defined above, the then function returns true. A simplified deadtime verification is applied to catch all possible effects. Otherwise, the function returns false.
4. If the neighborStripAboveThreshold function returns true, then it means that the neighbor strip was read out by its VMM and it should force the read out of the current strip. The under threshold digit is then saved to the outputDigits container.
5. Instead, if the neighborStripAboveThreshold function returns false, then repeat steps 1 to 4 for the below strip neighbor.

This general method of applying the neighborOn verification on below threshold strips provides the most efficient method of adding below threshold strips (tested to be factor 2 faster) and is also probably the safest method. Indeed, this method does not force the read out of neighboring strips (which may or may not have been "processed" by the VMM already) but instead just peaks at the neighbors for information on whether or not the given digit should be saved. In contrast, an implementation where we save the strips of the two neighbors makes the logic much more complicated and much harder to track and keep a valid VMM response: each strip digit is responsible of creating up to 3 digits per digit!

